import pandas as pd
import numpy as np
import os
from scipy.stats import sem




def equal_check(sats,pvm, number_of_bidders):
    for i in range(number_of_bidders):
        bundle_sats = sats[0][i]
        bundle_pvm = pvm[0][i]
        if (not (np.array_equal(bundle_sats, bundle_pvm))):
            # print("false")
            return False

    return True


path_to_directory = "/home/stkramer/bachelor_thesis/ba/lsvm_results"
number_of_bidders = 6
items = 18

Columns = ['filename', 'efficiency', 'min', '25%', '50%', '75%', 'standart_error', 'perfect_allocs', 'queries_avg', 'queries_max', 'iterations_avg', 'iterations_max', 'perfect_allocs_perc', 'revenue_capped', 'revenue', 'wrong allocs', 'not all allocated']
table = pd.DataFrame(columns=Columns)

for filename in os.listdir(path=path_to_directory):
    if (filename != 'summary_gsvm'):
        results = pd.read_csv(os.path.join(path_to_directory, filename))
        simulations = results.shape[0]
        print(f"simulations: {simulations}")
        

        mean_eff = results['efficiency'].mean()
        eff_quantiles = results["efficiency"].quantile([0.25, 0.5, 0.75])
        stand_err = sem(results['efficiency'])
        min_eff = results['efficiency'].min()
        print(eff_quantiles)
        print(type(eff_quantiles))
        mean_rev = results['revenue'].mean()
        mean_rev_capped = results['revenue_capped'].mean()
        mean_iter_avg = results['iter_average'].mean()
        mean_iter_max = results['iter_max'].mean()
        mean_queries_avg = results['avg_queries_per_bidder'].mean()
        mean_queries_max = results['avg_max_queries_per_bidder'].mean()
        # mean_time = results['runtime'].mean()

        # perfect allocations
        # LSVM
        sats_alloc = results.loc[:, ['bidder_0_sats', 'bidder_1_sats', 'bidder_2_sats', 'bidder_3_sats', 'bidder_4_sats', 'bidder_5_sats']]
        pvm_alloc = results.loc[:, ['bidder_0_pvm','bidder_1_pvm','bidder_2_pvm','bidder_3_pvm','bidder_4_pvm','bidder_5_pvm']]
        
        # GSVM
        # sats_alloc = results.loc[:, ['bidder_0_sats', 'bidder_1_sats', 'bidder_2_sats', 'bidder_3_sats', 'bidder_4_sats', 'bidder_5_sats', 'bidder_6_sats']]
        # pvm_alloc = results.loc[:, ['bidder_0_pvm','bidder_1_pvm','bidder_2_pvm','bidder_3_pvm','bidder_4_pvm','bidder_5_pvm', 'bidder_6_pvm']]

        # MRVM
        # sats_alloc = results.loc[:, ['bidder_0_sats', 'bidder_1_sats', 'bidder_2_sats', 'bidder_3_sats', 'bidder_4_sats', 'bidder_5_sats', 'bidder_6_sats', 'bidder_7_sats', 'bidder_8_sats', 'bidder_9_sats']]
        # pvm_alloc = results.loc[:, ['bidder_0_pvm','bidder_1_pvm','bidder_2_pvm','bidder_3_pvm','bidder_4_pvm','bidder_5_pvm', 'bidder_6_pvm', 'bidder_7_pvm', 'bidder_8_pvm', 'bidder_9_pvm']]

        count = 0
        not_all_allocated = 0
        wrong_allocs = 0
        for i in range(len(results)):
            # print(results.iloc[[i], [1]])
            sats = sats_alloc.iloc[[i], :]
            pvm = pvm_alloc.iloc[[i], :]
            allocated_items = []

            if (equal_check(sats.to_numpy(), pvm.to_numpy(), number_of_bidders)):
                # print(i)
                count += 1

            for i in range(number_of_bidders):
                tmp = eval(pvm.to_numpy()[0][i])
                allocated_items = allocated_items + tmp


            print(f"allocated items: {allocated_items}")

            if len(set(allocated_items)) < 18:
                not_all_allocated += 1

            if len(set(allocated_items)) != len(allocated_items):
                wrong_allocs += 1

            


        

        # print(count)

        res = {'filename' : filename[8:] ,'efficiency' : round(mean_eff,2), 'min' : round(min_eff,2), '25%' : round(eff_quantiles.iloc[0],2) , '50%' : round(eff_quantiles.iloc[1],2),  '75%' : round(eff_quantiles.iloc[2],2), 'standart_error' : round(stand_err,2), 'perfect_allocs' : count, 
        'queries_avg' : round(mean_queries_avg,2), 'queries_max' : round(mean_queries_max,2), 'iterations_avg' : round(mean_iter_avg,2), 'iterations_max' : round(mean_iter_max,2), 'perfect_allocs_perc' : round((1.0*count)/simulations * 100.0,2), 
        'revenue_capped' : round(mean_rev_capped,2), 'revenue' : round(mean_rev,2), 'wrong allocs' : wrong_allocs, 'not all allocated': not_all_allocated}
        table = table.append(res, ignore_index=True)

table.to_csv(os.path.join(path_to_directory, 'summary_lsvm.csv'))

