#!C:\Users\jakob\Anaconda3\pythonw.exe
# -*- coding: utf-8 -*-

"""
FILE DESCRIPTION: 

This file stores helper functions used across the files in this project.

"""

# Libs
from mimetypes import init
import numpy as np
import random
import re
import logging
from collections import OrderedDict
from docplex.mp.model import Model
import pandas as pd
from tensorflow.keras import models,layers,regularizers,optimizers
from tensorflow.keras.callbacks import ModelCheckpoint
import os
from sklearn import linear_model
from sklearn.svm import SVR

from source.nn import NN



__author__ = 'Jakob Weissteiner'
__copyright__ = 'Copyright 2019, Deep Learning-powered Iterative Combinatorial Auctions: Jakob Weissteiner and Sven Seuken'
__license__ = 'AGPL-3.0'
__version__ = '0.1.0'
__maintainer__ = 'Jakob Weissteiner'
__email__ = 'weissteiner@ifi.uzh.ch'
__status__ = 'Dev'

# %% (0) HELPER FUNCTIONS
# %%
def timediff_d_h_m_s(td):
    # can also handle negative datediffs
    if (td).days < 0:
        td = -td
        return -(td.days), -int(td.seconds/3600), -int(td.seconds/60)%60, -(td.seconds%60)
    return td.days, int(td.seconds/3600), int(td.seconds/60)%60, td.seconds%60

# %% Tranforms bidder_key to integer bidder_id
# key = valid bidder_key (string), e.g. 'Bidder_0'
def key_to_int(key):
        return(int(re.findall(r'\d+', key)[0]))

# %% PREPARE INITIAL BIDS FOR A SINGLE INSTANCE FOR ALL BIDDERS for MLCA MECHANISM
# THIS METHOD USES TRUE UNIFORM SAMPLING!
# SATS_auction_instance = single instance of a value model
# number_initial_bids = number of initial bids
# bidder_ids = bidder ids in this value model (int)
# scaler = scale the y values across all bidders, fit on the selected training set and apply on the validation set


def initial_bids_mlca_unif(SATS_auction_instance, number_initial_bids, bidder_names, scaler=None):
    initial_bids = OrderedDict()
    for bidder in bidder_names:
        logging.debug('Set up intial Bids for: %s', bidder)
        D = unif_random_bids(value_model=SATS_auction_instance, bidder_id=key_to_int(bidder), n=number_initial_bids)
        null = np.zeros(D.shape[1]).reshape(1, -1) # add null bundle
        D = np.append(D, null, axis=0)
        X = D[:, :-1]
        Y = D[:, -1]
        initial_bids[bidder] = [X, Y]
    if scaler is not None:
        tmp = np.array([])
        for bidder in bidder_names:
            tmp = np.concatenate((tmp, initial_bids[bidder][1]), axis=0)
        scaler.fit(tmp.reshape(-1, 1))
        logging.debug('')
        logging.debug('*SCALING*')
        logging.debug('---------------------------------------------')
        logging.debug('Samples seen: %s', scaler.n_samples_seen_)
        logging.debug('Data max: %s', scaler.data_max_)
        logging.debug('Data min: %s', scaler.data_min_)
        logging.debug('Scaling by: %s | %s==feature range max?', scaler.scale_, float(scaler.data_max_ * scaler.scale_))
        logging.debug('---------------------------------------------')
        initial_bids = OrderedDict(list((key, [value[0], scaler.transform(value[1].reshape(-1, 1)).flatten()]) for key, value in initial_bids.items()))
    return(initial_bids, scaler)

# %% PREPARE INITIAL BIDS FOR A SINGLE INSTANCE FOR ALL BIDDERS
# THIS METHOD USES TRUE UNIFORM SAMPLING!
# value_model = single instance of a value model
# c0 = number of initial bids
# bidder_ids = bidder ids in this value model (int)
# scaler = scale the y values across all bidders, fit on the selected training set and apply on the validation set


def initial_bids_pvm_unif(value_model, c0, bidder_ids, scaler=None):
    initial_bids = OrderedDict()
    for bidder_id in bidder_ids:
        logging.debug('Set up intial Bids for: Bidder_{}'.format(bidder_id))
        D = unif_random_bids(value_model=value_model, bidder_id=bidder_id, n=c0)
        # add zero bundle
        null = np.zeros(D.shape[1]).reshape(1, -1)
        D = np.append(D, null, axis=0)
        X = D[:, :-1]
        Y = D[:, -1]
        initial_bids['Bidder_{}'.format(bidder_id)] = [X, Y]
    if scaler is not None:
        tmp = np.array([])
        for bidder_id in bidder_ids:
            tmp = np.concatenate((tmp, initial_bids['Bidder_{}'.format(bidder_id)][1]), axis=0)
        scaler.fit(tmp.reshape(-1, 1))
        logging.debug('Samples seen: %s', scaler.n_samples_seen_)
        logging.debug('Data max: %s', scaler.data_max_)
        logging.debug('Data min: %s', scaler.data_min_)
        logging.debug('Scaling by: %s | %s==feature range max?', scaler.scale_, float(scaler.data_max_ * scaler.scale_))
        initial_bids = OrderedDict(list((key, [value[0], scaler.transform(value[1].reshape(-1, 1)).flatten()]) for key, value in initial_bids.items()))
    return([initial_bids, scaler])
# %% PREPARE INITIAL BIDS FOR A SINGLE INSTANCE FOR ALL BIDDERS
# THIS METHOD USES RANDOM SAMPLING OF BUNDLES FROM SATS VIA NORMAL DISTRIBUTION!
# value_model = single instance of a value model
# c0 = number of initial bids
# bidder_ids = bidder ids in this value model (int)
# scaler = scale the y values across all bidders, fit on the selected training set and apply on the validation set
# seed = seed corresponding to the bidder_ids


def initial_bids_pvm(value_model, c0, bidder_ids, scaler=None, seed=None):
    initial_bids = OrderedDict()
    for bidder_id in bidder_ids:
        logging.debug('Set up intial Bids for: Bidder_{}'.format(bidder_id))
        if seed is not None:
            D = np.array(value_model.get_random_bids(bidder_id=bidder_id, number_of_bids=c0, seed=seed[bidder_id]))
        else:
            D = np.array(value_model.get_random_bids(bidder_id=bidder_id, number_of_bids=c0))
        # add zero bundle
        null = np.zeros(D.shape[1]).reshape(1, -1)
        D = np.append(D, null, axis=0)
        X = D[:, :-1]
        Y = D[:, -1]
        initial_bids['Bidder_{}'.format(bidder_id)] = [X, Y]
    if scaler is not None:
        tmp = np.array([])
        for bidder_id in bidder_ids:
            tmp = np.concatenate((tmp, initial_bids['Bidder_{}'.format(bidder_id)][1]), axis=0)
        scaler.fit(tmp.reshape(-1, 1))
        logging.debug('Samples seen: %s', scaler.n_samples_seen_)
        logging.debug('Data max: %s', scaler.data_max_)
        logging.debug('Data min: %s', scaler.data_min_)
        logging.debug('Scaling by: %s', scaler.scale_, ' | ', float(scaler.data_max_ * scaler.scale_), '== feature range max?')
        initial_bids = OrderedDict(list((key, [value[0], scaler.transform(value[1].reshape(-1, 1)).flatten()]) for key, value in initial_bids.items()))
    return([initial_bids, scaler])
# %% This function formates the solution of the winner determination problem (WDP) given elicited bids.
# Mip = A solved DOcplex instance.
# elicited_bids = the set of elicited bids for each bidder corresponding to the WDP.
# bidder_names = bidder names (string, e.g., 'Bidder_1')
# fitted_scaler = the fitted scaler used in the valuation model.


def format_solution_mip_new(Mip, elicited_bids, bidder_names, fitted_scaler):
    tmp = {'good_ids': [], 'value': 0}
    Z = OrderedDict()
    for bidder_name in bidder_names:
        Z[bidder_name] = tmp
    S = Mip.solution.as_dict()
    for key in list(S.keys()):
        index = [int(x) for x in re.findall(r'\d+', str(key))]
        bundle = elicited_bids[index[0]][index[1], :-1]
        value = elicited_bids[index[0]][index[1], -1]
        if fitted_scaler is not None:
            logging.debug('*SCALING*')
            logging.debug('---------------------------------------------')
            logging.debug(value)
            logging.debug('WDP values for allocation scaled by: 1/%s',round(fitted_scaler.scale_[0],8))
            value = float(fitted_scaler.inverse_transform([[value]]))
            logging.debug(value)
            logging.debug('---------------------------------------------')
        bidder = bidder_names[index[0]]
        Z[bidder] = {'good_ids': list(np.where(bundle == 1)[0]), 'value': value}
    return(Z)
# %% This function generates bundle-value pairs for a single bidder sampled uniformly at random from the bundle space.
# value_model = SATS auction model instance generated via PySats
# bidder_id = bidder id (int)
# n = number of bundle-value pairs.


def unif_random_bids(value_model, bidder_id, n):
    logging.debug('Sampling uniformly at random %s bundle-value pairs from bidder %s',n, bidder_id)
    ncol = len(value_model.get_good_ids())  # number of items in value model
    D = np.unique(np.asarray(random.choices([0,1], k=n*ncol)).reshape(n,ncol), axis=0)
    # get unique ones if accidently sampled equal bundle
    while D.shape[0] != n:
        tmp = np.asarray(random.choices([0,1], k=ncol)).reshape(1, -1)
        D = np.unique(np.vstack((D, tmp)), axis=0)
    # define helper function for specific bidder_id
    def myfunc(bundle):
        return value_model.calculate_value(bidder_id, bundle)
    D = np.hstack((D, np.apply_along_axis(myfunc, 1, D).reshape(-1, 1)))
    del myfunc
    # print(D)
    return(D)


# Active Learning function for start samples

# Greedy Sampling on the inputs

def initial_bids_greedy_input(value_model, c0, bidder_ids, starting_samples, scaler):
    initial_bids = OrderedDict()
    number_of_items = len(value_model.get_good_ids())
    for bidder_id in bidder_ids:
        print(('Set up intial Bids for: Bidder_{}'.format(bidder_id)))

        # Setting up the starting samples uniform at random
        D = np.unique(np.asarray(random.choices([0,1], k=starting_samples*number_of_items)).reshape(starting_samples,number_of_items), axis=0)
        while D.shape[0] != starting_samples:
            tmp = np.asarray(random.choices([0,1], k=number_of_items)).reshape(1, -1)
            D = np.unique(np.vstack((D, tmp)), axis=0)

        # setting up the remaining initial bids using Greedy Sampling on the inputs
        remaining_samples = c0 -starting_samples
        while D.shape[0] <= remaining_samples:
            max_distance = -1
            max_bundle = None
            for row in D:
                distance, bundle = solveLP(row, D)
                if (distance > max_distance):
                    max_bundle = np.array(bundle)
                    max_distance = distance
            max_bundle = convert_to_binary(max_bundle)
            # print('Add new bundle: {}'.format(max_bundle))
            D = np.unique(np.vstack((D, max_bundle.reshape(1, -1))), axis=0)

        def myfunc(bundle):
            return value_model.calculate_value(bidder_id, bundle)
        D = np.hstack((D, np.apply_along_axis(myfunc, 1, D).reshape(-1, 1)))
        del myfunc
        # add zero bundle
        null = np.zeros(D.shape[1]).reshape(1, -1)
        D = np.unique(np.append(D, null, axis=0), axis=0)
        # print(D)
        X = D[:, :-1]
        Y = D[:, -1]
        initial_bids['Bidder_{}'.format(bidder_id)] = [X, Y]

    if scaler is not None:
        tmp = np.array([])
        for bidder_id in bidder_ids:
            tmp = np.concatenate((tmp, initial_bids['Bidder_{}'.format(bidder_id)][1]), axis=0)
        scaler.fit(tmp.reshape(-1, 1))
        logging.debug('Samples seen: %s', scaler.n_samples_seen_)
        logging.debug('Data max: %s', scaler.data_max_)
        logging.debug('Data min: %s', scaler.data_min_)
        logging.debug('Scaling by: %s', scaler.scale_, ' | ', float(scaler.data_max_ * scaler.scale_), '== feature range max?')
        initial_bids = OrderedDict(list((key, [value[0], scaler.transform(value[1].reshape(-1, 1)).flatten()]) for key, value in initial_bids.items()))

    return([initial_bids, scaler])

# Helper function for greedy sampling on inputs. Constructs a LP, that should find a not already sampled bundle, which is the furthest away from the initial_bundle, but closer to the initial_bundle
# than to any other sampled bundle. 
def solveLP(initial_bundle, D):
    number_of_items = len(initial_bundle)
    model = Model(name='find_next_bundle')
    x = model.binary_var_list(number_of_items, name="x")
    
    for row in D:
        model.add_constraint(sum(row[i] + x[i] - 2*row[i]*x[i] for i in range(number_of_items)) >= sum(initial_bundle[i] + x[i] - 2*initial_bundle[i]*x[i] for i in range(number_of_items)))

    model.set_objective("max", sum(initial_bundle[i] + x[i] - 2*initial_bundle[i]*x[i] for i in range(number_of_items)))
    sol = model.solve()

    if (sol != None):
        distance = sol.get_objective_value()
        # print(distance)
        bundle = sol.get_value_list(x)
        # print(bundle)
        return distance, bundle
    else:
        print("wasn't solved!")
        return 0, None


def initial_bids_greedy_output_quad(value_model, c0, bidder_ids, starting_samples, scaler):
    initial_bids = initial_bids_greedy_input(value_model=value_model, c0=starting_samples, bidder_ids=bidder_ids, starting_samples=1, scaler=scaler)[0]
    number_of_items = len(value_model.get_good_ids())
    for bidder_id in bidder_ids:
        D = initial_bids['Bidder_{}'.format(bidder_id)]
        train_x = D[0]
        train_y = D[1]


        while (len(train_y) <= c0 ):
            model_svr = SVR(kernel='poly', degree=2)
            model_svr.fit(train_x, train_y)
            weights = model_svr.dual_coef_
            print(weights)
            print(type(weights))
            support_vectors = model_svr.support_vectors_
            max_distance = -1
            max_bundle = None
            for i in range(len(train_y)):
            # print("bundle" + str(train_x[i]))
                distance, bundle = solveLP_3(weights=weights, bundle_y=train_y[i], queried_bundles=train_y, number_of_items=number_of_items, support_vectors=support_vectors)
                if (distance > max_distance):
                    max_bundle = np.array(bundle)
                    max_distance = distance
            
            if (max_bundle is not None):
                # print(type(max_bundle))
                max_bundle = convert_to_binary(max_bundle)
                train_x = np.vstack((train_x, max_bundle))
                # print(train_x.shape)
                # print(train_x)
                y_value = value_model.calculate_value(bidder_id, max_bundle)
                train_y = np.append(train_y, y_value)
                #print(train_y)
                #print(train_x)
                print('Add new bundle: {}'.format(max_bundle))


    initial_bids['Bidder_{}'.format(bidder_id)] = [train_x, train_y]
    # print(initial_bids)
    return [initial_bids, scaler]



def initial_bids_greedy_output_linear(value_model, c0, bidder_ids, starting_samples, scaler):
    res = initial_bids_greedy_input(value_model=value_model, c0=starting_samples, bidder_ids=bidder_ids, starting_samples=1, scaler=scaler)
    initial_bids = res[0]
    scaler = res[1]
    number_of_items = len(value_model.get_good_ids())
    for bidder_id in bidder_ids:
        D = initial_bids['Bidder_{}'.format(bidder_id)]
        train_x = D[0]
        train_y = D[1]
        print("first phase done " + str(bidder_id))

        while (len(train_y) <= c0):
            
            model = linear_model.Ridge(alpha=0.5, positive=True)
            model.fit(train_x, train_y)
            weights = model.coef_

            max_distance = -1
            max_bundle = None

            for i in range(len(train_y)):
                # print("bundle" + str(train_x[i]))
                distance, bundle = solveLP_linear(weights=weights, bundle_y=train_y[i], queried_bundles=train_y, number_of_items=number_of_items)
                if (distance > max_distance):
                    max_bundle = np.array(bundle)
                    max_distance = distance
            
            if (max_bundle is not None):
                max_bundle = convert_to_binary(max_bundle)
                train_x = np.vstack((train_x, max_bundle))

                y_value = value_model.calculate_value(bidder_id, max_bundle)

                print(bidder_id)
                print("value before: " + str(y_value))
                if scaler is not None:
                    logging.debug('Queried Value scaled by: %s', scaler.scale_)
                    y_value = float(scaler.transform([[y_value]]))
                    print("value after: " + str(y_value))

                train_y = np.append(train_y, y_value)
                print(train_y)
                #print(train_x)
                # print('Add new bundle: {}'.format(max_bundle))
                # print("value: ", y_value)

        # print(train_x)
        # print(train_y)
        initial_bids['Bidder_{}'.format(bidder_id)] = [train_x, train_y]
    print(initial_bids)
    return [initial_bids, scaler]

  
def solveLP_linear(weights, bundle_y, queried_bundles, number_of_items):
    model = Model(name='find_next_bundle')
    x = model.binary_var_list(number_of_items, name="x")
    w_1 = model.continuous_var(name="w_1")
    w_2 = model.continuous_var(name="w_2")
    # b = model.binary_var(name="b")
    d = model.binary_var_list(len(queried_bundles), name="d")
    constant = 2**20

    model.add_constraint(bundle_y - sum(weights[i] * x[i] for i in range(number_of_items)) <= w_1)
    model.add_constraint(sum(weights[i] * x[i] for i in range(number_of_items)) - bundle_y <= w_1)
    model.add_constraint(w_1 <= w_2)
    model.add_constraint(w_1 >= 0.0)

    j = 0
    for value in queried_bundles:
        model.add_constraint(value - sum(weights[i] * x[i] for i in range(number_of_items)) + constant * d[j] >= w_2 )
        model.add_constraint(sum(weights[i] * x[i] for i in range(number_of_items)) - value + constant * (1-d[j]) >= w_2)
        j+=1
        

    # old constraints --> wrong!!
    # model.add_constraint(bundle_y - sum(weights[i] * x[i] for i in range(number_of_items)) + constant * b >= w_1)
    # model.add_constraint(sum(weights[i] * x[i] for i in range(number_of_items)) - bundle_y + constant * (1-b) >= w_1)
    # model.add_constraint(w_1 <= w_2)

    # j = 0
    # for value in queried_bundles:
        # model.add_constraint(value - sum(weights[i] * x[i] for i in range(number_of_items)) + constant * d[j] >= w_2 )
        # model.add_constraint(sum(weights[i] * x[i] for i in range(number_of_items)) - value + constant * (1-d[j]) >= w_2)
        # j+=1

    model.set_objective("max", w_1)
    
    sol = model.solve()
    if (sol != None):
        distance = sol.get_objective_value()
        # print("distance: ", distance)
        # print(queried_bundles)
        bundle = sol.get_value_list(x)
        # print("w_2: ", sol.get_value(w_2))
        # print("b: ", sol.get_value(b))
        # print("bundle_y: ", bundle_y)
        # print("found bundle: ", bundle)
        # print("value of found bundle: ", sum(weights[i] * bundle[i] for i in range(number_of_items)))
        del model
        return distance, bundle
    else:
        del model
        print("wasn't solved!")
    return 0, None




def solveLP_3(weights, bundle_y, queried_bundles, number_of_items, support_vectors):
    model = Model('find_next_bundle')
    x = model.binary_var_list(number_of_items, name="x")
    w_1 = model.continuous_var(name="w_1")
    w_2 = model.continuous_var(name="w_2")
    b = model.binary_var(name="b")
    d = model.binary_var_list(len(queried_bundles), name="d")
    constant = 2**20

    precomputed_taus = np.arange(10)
    precomputed_taus = precomputed_taus**2
    tau_max_list = []

    j=0
    print(support_vectors)
    for sv in support_vectors:
        tau_max = int (np.sum(sv))
        tau_max_list.append(tau_max)
        G = model.binary_var_list(tau_max + 1, name=("G_sv_" + str(j)))
        model.add_constraint(sum(G[i] for i in range(tau_max + 1)) == 1)
        model.add_constraint(sum(x[i] for i in range(number_of_items)) == sum((i+1)*G[i] for i in range(tau_max + 1))-1)

        j = j+1
    
    number_of_sv = j
    print(number_of_sv)

    for j in range(number_of_sv):
        print(j)    
        G = model.get_var_by_name(name="G_sv_" + str(j) + "_0")
        print(G)
        print(type(G))

    v_x = model.continuous_var(name="v_x")
    model.add_constraint(sum((sum(model.get_var_by_name("G_sv_" + str(i) + "_" + str(j)) for j in range(tau_max_list[i] + 1))) for i in range(number_of_sv)) == v_x)
    
    model.add_constraint(bundle_y - v_x + constant * b >= w_1)
    model.add_constraint(v_x - bundle_y + constant * (1-b) >= w_1)
    model.add_constraint(w_1 <= w_2)

    for value in queried_bundles:
        model.add_constraint(value - v_x + constant * b >= w_2)
        model.add_constraint(v_x - value + constant * (1-b) >= w_2)

    model.set_objective("max", w_1)
    
    sol = model.solve()
    if (sol != None):
        distance = sol.get_objective_value()
        # print(distance)
        bundle = sol.get_value_list(x)
        # print(bundle)
        return distance, bundle
    else:
        print("wasn't solved!")
    return 0, None

def initial_bids_greedy_output_nn(value_model, c0, bidder_ids, starting_samples, parameters, scaler, epochs, batch_size, big_L):

    initial_bids = initial_bids_greedy_input(value_model=value_model, c0=starting_samples, bidder_ids=bidder_ids, starting_samples=1, scaler=scaler)[0]
    number_of_items = len(value_model.get_good_ids())
    for bidder_id in bidder_ids:
        D = initial_bids['Bidder_{}'.format(bidder_id)]
        train_x = D[0]
        train_y = D[1]
        print("first phase done " + str(bidder_id))

        while(len(train_y) <= c0):
            print(train_y)
            data = np.hstack((train_x, train_y.reshape(len(train_y),1)))
            # print("data: ", data)
            np.random.shuffle(data)
            # print("data after shuffle: ", data)
            middle = int (len(train_y) * 0.8)
            # print("middle: ", middle)
            data_train = data[:middle, :]
            data_valid = data[middle:, :]
            # print("train_x: ", data_train[:, :-1])
            # print("train_y: ", data_valid[:, [-1]].flatten())

            filepath = os.getcwd() + "/save_model/"
            # print("filename: ", filepath)
            # print(type(filepath))
            checkpoint = ModelCheckpoint(filepath=filepath, monitor='val_loss', verbose=0, 
                             save_best_only=True, save_weights_only=True, 
                             mode='auto', save_frequency=1)


            model = NN(model_parameters=parameters, X_train=data_train[:,:-1], Y_train=data_train[:, [-1]].flatten(), scaler=scaler)
            # model = NN(model_parameters=parameters, X_train=train_x, Y_train=train_y, scaler=scaler)
            model.initialize_model()
            model.find_best_model(epochs=epochs, batch_size=batch_size, X_valid=data_valid[:, :-1], Y_valid=data_valid[:, [-1]].flatten(), checkpoint=checkpoint)
            # model.fit(epochs=epochs, batch_size=batch_size)

            model.model.load_weights(os.getcwd() + "/save_model/")

            # score = model.model.evaluate(data_valid[:, :-1], data_valid[:, [-1]].flatten(), verbose=2)
            # print("test loss: ", score)
            # print("prediction: ", model.model.predict(train_x))
            # print("true value: ", train_y)
            # print("prediction valid set: ", model.model.predict(data_valid[:,:-1]))
            # print("true value: ", data_valid[:, [-1]].flatten())


            # print("prediction train set: ", model.model.predict(data_train[:,:-1]))
            # print("true value: ", data_train[:, [-1]].flatten())
            
            
            max_bundle = None
            max_distance = -1
            for i in range(len(train_y)):
                # print("bundle" + str(train_x[i]))
                distance, bundle = solveLP_NN(weights=model.model.get_weights(), bundle_y=train_y[i], queried_bundles_y=train_y, number_of_items=number_of_items, big_L=big_L, queried_bundles_x=train_x)
                if (distance > max_distance):
                    max_bundle = np.array(bundle)
                    max_distance = distance

            if ((max_bundle is not None)):
                max_bundle = convert_to_binary(max_bundle)
                train_x = np.vstack((train_x, max_bundle))

                y_value = value_model.calculate_value(bidder_id, max_bundle)

                
                if scaler is not None:
                    logging.debug('Queried Value scaled by: %s', scaler.scale_)
                    y_value = float(scaler.transform([[y_value]]))

                train_y = np.append(train_y, y_value)

                print('Add new bundle: {}'.format(max_bundle))
                print("estimated value: ", model.model.predict(np.array([max_bundle])))
                print("value: ", y_value)
            else:
                print("no max_bundle was found")
                raise Exception()
        
        # print("length: ", len(train_y))
        # print(train_y)
        initial_bids['Bidder_{}'.format(bidder_id)] = [train_x, train_y]
    print(initial_bids)
    return [initial_bids, scaler]
        


def solveLP_NN(weights, bundle_y, queried_bundles_y, queried_bundles_x, number_of_items, big_L):
    Wb = _clean_weights(weights)

    model = Model('find_next_bundle')
    x = model.binary_var_list(number_of_items, name="x")
    w_1 = model.continuous_var(name="w_1")
    w_2 = model.continuous_var(name="w_2")
    b = model.binary_var(name="b")
    d = model.binary_var_list(len(queried_bundles_y), name="d")
    constant = big_L
    

    layer = 1
    for v in range(0, len(Wb), 2):
        W = Wb[v].transpose()
        b_vector = Wb[v+1]
        rows, cols = W.shape

        if (v == 0):
            z_0 = model.continuous_var_list(number_of_items, name="z_0")
            model.add_constraints((z_0[i] == x[i] for i in range(number_of_items)))

        # variables for the layer
        z_tmp = model.continuous_var_list(rows, name="z_" + str(layer))
        s_tmp = model.continuous_var_list(rows, name="s_" + str(layer))
        y_tmp = model.binary_var_list(rows, name="y_" + str(layer)) 

        # constraints for the layer
        model.add_constraints(z_tmp[i] >= 0 for i in range(rows))
        model.add_constraints(z_tmp[i] <= y_tmp[i] * big_L for i in range(rows))
        model.add_constraints(s_tmp[i] >= 0 for i in range(rows))
        model.add_constraints(s_tmp[i] <= (1 - y_tmp[i]) * big_L for i in range(rows))
        model.add_constraints(z_tmp[i] - s_tmp[i] == sum(W[i,c] * model.get_var_by_name("z_" + str(layer-1) + "_" + str(c)) for c in range(cols)) + b_vector[i] for i in range(rows))

        layer = layer + 1
    
    # set v_x equal to the value of the output-layer
    v_x = model.continuous_var(name="v_x")
    model.add_constraint(v_x == model.get_var_by_name("z_" + str(layer-1) + "_0"))

    # constraints to find the bundle with largest distance to its closest neighbor, that has already been queried
    model.add_constraint(bundle_y - v_x <= w_1)
    model.add_constraint(v_x - bundle_y <= w_1)
    model.add_constraint(w_1 <= w_2)
    model.add_constraint(w_1 >= 0)
    
    i = 0
    for value in queried_bundles_y: # constraints to guarantee, that no other queried bundle lays closer to the value of x than bundle_y
        model.add_constraint(value - v_x + constant * d[i] >= w_2)
        model.add_constraint(v_x - value + constant * (1-d[i]) >= w_2)
        i = i + 1

    # ensures, that no bundle, that was already queried, will be queried again
    for bundle in queried_bundles_x:
        model.add_constraint(sum(x[i] + bundle[i] - 2 * x[i] * bundle[i] for i in range(number_of_items)) >= 1)

    model.set_objective("max", w_1)
    
    sol = model.solve()
    if (sol != None):
        distance = sol.get_objective_value()
        # print("distance: ", distance)
        # print(queried_bundles_y)
        bundle = sol.get_value_list(x)
        # print("bundle_y: ", bundle_y)
        # print("queried bundles: ", queried_bundles_x)
        # print("found bundle: ", bundle)
        # print("estimated value: " ,sol.get_value(v_x))
        return distance, bundle
    else:
        # print("wasn't solved!")
        return -1, None    


def initial_bids_al_input_uf_combined(value_model, c0, bidder_ids, starting_samples, scaler):
    
    initial_bids = initial_bids_greedy_input(value_model=value_model, c0=starting_samples, bidder_ids=bidder_ids, starting_samples=1, scaler=scaler)[0]
    number_of_items = len(value_model.get_good_ids())

    for bidder_id in bidder_ids:
        D = initial_bids['Bidder_{}'.format(bidder_id)][0]
        # print("bids at start")
        # print(initial_bids['Bidder_{}'.format(bidder_id)])
        while D.shape[0] < c0:
            tmp = np.asarray(random.choices([0,1], k=number_of_items)).reshape(1, -1)
            D = np.unique(np.vstack((D, tmp)), axis=0)
    # define helper function for specific bidder_id
        def myfunc(bundle):
            return value_model.calculate_value(bidder_id, bundle)
        D = np.hstack((D, np.apply_along_axis(myfunc, 1, D).reshape(-1, 1)))
        del myfunc

        # print("bids at the end")
        # print(D)

        X = D[:, :-1]
        Y = D[:, -1]
        initial_bids['Bidder_{}'.format(bidder_id)] = [X, Y]

    if scaler is not None:
        tmp = np.array([])
        for bidder_id in bidder_ids:
            tmp = np.concatenate((tmp, initial_bids['Bidder_{}'.format(bidder_id)][1]), axis=0)
        scaler.fit(tmp.reshape(-1, 1))
        logging.debug('Samples seen: %s', scaler.n_samples_seen_)
        logging.debug('Data max: %s', scaler.data_max_)
        logging.debug('Data min: %s', scaler.data_min_)
        logging.debug('Scaling by: %s', scaler.scale_, ' | ', float(scaler.data_max_ * scaler.scale_), '== feature range max?')
        initial_bids = OrderedDict(list((key, [value[0], scaler.transform(value[1].reshape(-1, 1)).flatten()]) for key, value in initial_bids.items()))

    print(initial_bids)

    return [initial_bids, scaler]



def _clean_weights(Wb):
    for v in range(0, len(Wb)-2, 2):
        Wb[v][abs(Wb[v]) <= 1e-8] = 0
        Wb[v+1][abs(Wb[v+1]) <= 1e-8] = 0
        zero_rows = np.where(np.logical_and((Wb[v] == 0).all(axis=0), Wb[v+1] == 0))[0]
        if len(zero_rows) > 0:
            logging.debug('Clean Weights (rows) %s', zero_rows)
            Wb[v] = np.delete(Wb[v], zero_rows, axis=1)
            Wb[v+1] = np.delete(Wb[v+1], zero_rows)
            Wb[v+2] = np.delete(Wb[v+2], zero_rows, axis=0)
    return(Wb)

def convert_to_binary(array):

    for i in range(len(array)):
        if (abs(1 - array[i]) < 0.01):
            array[i] = 1.0
        else:
            array[i] = 0
    return array


def make_bids_binary(elicited_bids, bidder_ids, scaler):
    binary_elicited_bids = OrderedDict()
    new_bundles = []

    # print(type(elicited_bids))
    # print(elicited_bids)

    # print(type(elicited_bids))
    # print(type(elicited_bids[0]))
    all_bids = elicited_bids[0]

    for key,value in all_bids.items():
        # print(key)
        bundles = value[0]
        # print(bundles)

        for bundle in bundles:
            new_bundle = convert_to_binary(bundle)
            new_bundles = np.append(new_bundles, new_bundle)

        binary_elicited_bids[key] = [bundles, value[1]]

    # for bidder_id in bidder_ids:
        # elicited_bids_of_bidder = elicited_bids['Bidder_{}'.format(bidder_id)]
        # for bundle in elicited_bids_of_bidder[0]:
            # new_bundle = convert_to_binary(bundle)
            # bundles = np.append(bundles, new_bundle)
        # binary_elicited_bids['Bidder_{}'.format(bidder_id)] = [bundles, elicited_bids_of_bidder[1]]

    # print(binary_elicited_bids)
    return [binary_elicited_bids, scaler]

# returns true if any item is allocated to at most one user, False otherwise
def check_validity_allocation(allocation):
    items = list()
    for key, value in allocation.items():
        items = items + value['good_ids']
    
    items_set = set(items)
    print("items: ", items)
    print("items_set: ", items_set)
    return len(items) == len(items_set)

# alloc has to be a two dimensional np-array --> rows: bidder, columns: items
# will check if the allocation has set exactly one entry to 1 and all others to 0
def check_argmax_allocation(alloc):
    for column in alloc.T:
        tmp = 0
        for elem in column:
            if elem != 0:
                tmp += 1
        if tmp > 1:
            print(alloc)
            raise ValueError()
