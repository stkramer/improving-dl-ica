# Improving DL-ICA


The most part of this code was written by Jakob Weissteiner and Sven Seuken. If you use this software for academic purposes, please cite the above in your work. Bibtex for this reference is as follows:

```tex
@InProceedings{weissteiner2020deep,
    author = {Weissteiner, Jakob and Seuken, Sven},
    title = {Deep Learning-powered Iterative Combinatorial Auctions},
    booktitle = {Proceedings of the 34th AAAI Conference on Artificial Intelligence (AAAI-20).},
    year = {2020},
}
```

## Requirements

* Python 3.6
* Java 8 (or later)
  * Java environment variables set as described [here](https://pyjnius.readthedocs.io/en/stable/installation.html#installation)
* JAR-files ready (they should already be)
  * CPLEX (>=12.8.0): The file cplex.jar is provided in source/lib.
  * [SATS](http://spectrumauctions.org/) (>=0.6.4): The file sats.jar is provided in source/lib.
* CPLEX Python API installed as described [here](https://www.ibm.com/support/knowledgecenter/SSSA5P_12.8.0/ilog.odms.cplex.help/CPLEX/GettingStarted/topics/set_up/Python_setup.html)


## Dependencies

It is recommended to run the code in a python environment (either with conda or virtualenv). 
The necessary dependencies/libraries for the environment can be found in the file 'requirements.txt.


## Example: How to run PVM for GSVM

Open the file 'example_pvm_main_gsvm.py'. We start by setting the parameters of the GSVM model. 

```python
model_name = 'GSVM'
N = 7  # number of bidders
M = 18  # number of items
bidder_types = 2  # regional and national bidders
bidder_ids = list(range(0, 7))  # bidder ids
scaler = False  # no scaling of bidders valuations
```

It should be noted that these values are fixed for the GSVM model and therefore shouldn't be changed. Otherwise it wouldn't represent the actual GSVM model. 

Further, we can define the parameter of the neural networks. Here, we are free to change the parameters. A possible setting for GSVM would be the following. 

```python
epochs = 512
batch_size = 32
regularization_type = 'l2'  # 'l1', 'l2' or 'l1_l2'
# national bidder LSVM: id=0, GSVM:id=6, MRVM:id=7,8,9
regularization_N = 0.00001
learning_rate_N = 0.01
layer_N = [10, 10]
dropout_N = True
dropout_prob_N = 0.05
# regional bidders LSVM: id=1-5, GSVM:id=0-5, MRVM:id=3,4,5,6
regularization_R = 0.00001
learning_rate_R = 0.01
layer_R = [32, 32]
dropout_R = True
dropout_prob_R = 0.05
```

All parameters ending with R define the neural networks of the regional bidders, the ones ending with N the neural network of the national bidder. 

Finally we can also the define parameters for the mixed integer program: 

```python
L = 3000  # global big-M constant
Mip_bounds_tightening = 'IA'   # Bound tightening: False ,'IA' or 'LP'
warm_start = True  # boolean, should previous solution be used as a warm start.
```

The file can then be run in the console as follows:

python3 ABSOLUTE_PATH_TO_FILE/example_pvm_main_lsvm.py i c_{0} c_{e} DIB sampling_technique additional_queries file

The seven attributes can be chosen as follows:
- i: integer between 0 and 99. To choose the seed instance. 
- c_{0}: number of initial bundles (positive integer)
- c_{e}: upper bound for number of queried bundles in the iterative phase (postive integer)
- DIB: either 0 or 1. If 0, all economies use the same initial bundles. If 1, the economies use different initial bundles
- sampling_technique: Defines the used sampling technique for the initial bundles. Possible are: 'uf' for Uniform Sampling, 'al_input' for greedy active learning on input values, 'al_output_linear' for greedy active learning on output values with a linear model, 'al_output_nn' for greedy active learning on output values with a neural network and 'combined_al_input_uf' for a combination of greedy active learning on input values and uniform sampling. 
- additional_queries: denotes how many bidders are additionally queried per allocated bundle. Integer between 0 and n (n-1 = number of bidders)
- file: csv-file to store the results

A possible command would be:

python3 ABSOLUTE_PATH_TO_FILE/example_pvm_main_lsvm.py 5 30 20 0 'al_input' 0 'results.csv'