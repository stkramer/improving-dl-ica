import os
from pathlib import Path
import jnius_config

file_path = os.path.join(
    Path(__file__).parent.absolute().parent.absolute(), "/home/stkramer/bachelor_thesis/ba/", "lib", "*"
)
jnius_config.set_classpath(".", file_path)
print(file_path)
