#!C:\Users\jakob\Anaconda3\pythonw.exe
# -*- coding: utf-8 -*-

"""
FILE DESCRIPTION:
This file presents an example of how to run the PVM mechanism wit the function pvm() from the file pvm.py
"""

# Libs
import logging
from sklearn.preprocessing import MinMaxScaler  # used only for MRVM
import pandas as pd
import random
import tensorflow as tf
import numpy as np
import sys

import os
from pathlib import Path
import jnius_config

file_path = os.path.join(
    # Path(__file__).parent.absolute().parent.absolute(), "/home/stkramer/bachelor_thesis/ba/", "lib", "*" # for arton cluster
    Path(__file__).parent.absolute().parent.absolute(), "/home/stkramer/bachelor_thesis/ba", "lib", "*" # for arton cluster
)
jnius_config.set_classpath(".", file_path)
print(file_path)

# Own Modules
from source.pvm import pvm

__author__ = 'Jakob Weissteiner'
__copyright__ = 'Copyright 2019, Deep Learning-powered Iterative Combinatorial Auctions: Jakob Weissteiner and Sven Seuken'
__license__ = 'AGPL-3.0'
__version__ = '0.1.0'
__maintainer__ = 'Jakob Weissteiner'
__email__ = 'weissteiner@ifi.uzh.ch'
__status__ = 'Dev'

# COLUMNS = ['seed_instance', 'efficiency', 'pay_bidder_0', 'pay_bidder_1', 'pay_bidder_2', 'pay_bidder_3', 'pay_bidder_4',
# 'pay_bidder_5', 'revenue', 'revenue_capped', 'iter_max', 'iter_average', 'runtime', 'average_MIP_time', 'median_MIP_time', 
# 'avg_queries_per_bidder', 'avg_max_queries_per_bidder', 'SATS_value', 'percentage_regio_sats', 'percentage_national_sats', 
# 'pvm_allocation_value', 'percentage_regio_pvm', 'percentage_national_pvm']

COLUMNS_GSVM = ['seed_instance', 'efficiency', 'pay_bidder_0', 'pay_bidder_1', 'pay_bidder_2', 'pay_bidder_3', 'pay_bidder_4',
'pay_bidder_5', 'pay_bidder_6', 'revenue', 'revenue_capped', 'iter_max', 'iter_average', 
'runtime', 'average_MIP_time', 'median_MIP_time', 'avg_queries_per_bidder', 'avg_max_queries_per_bidder', 'SATS_value', 
'percentage_regio_sats', 'percentage_national_sats', 'bidder_0_sats', 'bidder_1_sats', 'bidder_2_sats', 'bidder_3_sats', 
'bidder_4_sats', 'bidder_5_sats', 'bidder_6_sats', 'pvm_allocation_value', 'percentage_regio_pvm', 'percentage_national_pvm', 'bidder_0_pvm', 
'bidder_1_pvm', 'bidder_2_pvm', 'bidder_3_pvm', 'bidder_4_pvm','bidder_5_pvm', 'bidder_6_pvm']


COLUMNS_LSVM = ['seed_instance', 'efficiency', 'pay_bidder_0', 'pay_bidder_1', 'pay_bidder_2', 'pay_bidder_3', 'pay_bidder_4',
'pay_bidder_5', 'revenue', 'revenue_capped', 'iter_max', 'iter_average', 
'runtime', 'average_MIP_time', 'median_MIP_time', 'avg_queries_per_bidder', 'avg_max_queries_per_bidder', 'SATS_value', 
'percentage_regio_sats', 'percentage_national_sats', 'bidder_0_sats', 'bidder_1_sats', 'bidder_2_sats', 'bidder_3_sats', 
'bidder_4_sats', 'bidder_5_sats', 'pvm_allocation_value', 'percentage_regio_pvm', 'percentage_national_pvm', 'bidder_0_pvm', 
'bidder_1_pvm', 'bidder_2_pvm', 'bidder_3_pvm', 'bidder_4_pvm','bidder_5_pvm']


COLUMNS_MRVM = ['seed_instance', 'efficiency', 'pay_bidder_0', 'pay_bidder_1', 'pay_bidder_2', 'pay_bidder_3', 'pay_bidder_4',
'pay_bidder_5', 'pay_bidder_6', 'pay_bidder_7', 'pay_bidder_8', 'pay_bidder_9', 'revenue', 'revenue_capped', 'iter_max', 'iter_average', 
'runtime', 'average_MIP_time', 'median_MIP_time', 'avg_queries_per_bidder', 'avg_max_queries_per_bidder', 'SATS_value', 
'percentage_regio_sats', 'percentage_national_sats', 'percentage_local_sats', 'bidder_0_sats', 'bidder_1_sats', 'bidder_2_sats', 'bidder_3_sats', 
'bidder_4_sats', 'bidder_5_sats', 'bidder_6_sats', 'bidder_7_sats', 'bidder_8_sats', 'bidder_9_sats','pvm_allocation_value', 
'percentage_regio_pvm', 'percentage_national_pvm', 'percentage_local_pvm', 'bidder_0_pvm', 'bidder_1_pvm', 'bidder_2_pvm', 'bidder_3_pvm', 'bidder_4_pvm',
'bidder_5_pvm', 'bidder_6_pvm', 'bidder_7_pvm', 'bidder_8_pvm', 'bidder_9_pvm']

# Random Seeds:
# rng = random.Random(init_seed)
tf.random.set_seed(0)



# %% define logger
# clear existing logger
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)
# LOG DEBUG TO CONSOLE
logging.basicConfig(filename='debug.log', level=logging.DEBUG, format='%(message)s', filemode='w')
# %% Define parameters
simulations = 1
results = pd.DataFrame(columns=COLUMNS_LSVM)
seeds = [8147,2930,2957,6968,4692,102,7092,9841,7701,7746,2834,3825,9625,6331,4557,
799,2238,614,1490,9881,6264,4057,8608,7408,3844,8396,2829,6994,3954,4425,7202,9837,
9346,5589,412,2138,9886,9080,8107,5028,4719,7452,2099,265,4541,660,547,7893,5692,6471]
seed = seeds[(int)(sys.argv[1])]


# (1) Value model parameters

# logging.basicConfig(filename="debug" + str(i) + ".log", level=logging.DEBUG, format='%(message)s', filemode='w')
# i += 1
results = pd.read_csv('/home/stkramer/bachelor_thesis/ba/results_lsvm_30_ali_sib.csv').iloc[:,1:]
model_name = 'LSVM'
N = 6  # number of bidders
M = 18  # number of items
bidder_types = 2
bidder_ids = list(range(0, 6))
scaler = False
print('\n------------------------ SATS parameters ------------------------')
print('Value Model:', model_name)
print('Number of Bidders: ', N)
print('Number of BidderTypes: ', bidder_types)
print('Number of Items: ', M)
print('Scaler: ', scaler)
# 
# =============================================================================
# GSVM
# model_name = 'GSVM'
# N = 7  # number of bidders
# M = 18  # number of items
# bidder_types = 2
# bidder_ids = list(range(0, 7))
# scaler = False
# print('\n------------------------ INFO  PVM RUN ------------------------')
# print('Value Model: ', model_name)
# print('Number of Bidders: ', N)
# print('Number of BidderTypes: ', bidder_types)
# print('Number of Items: ', M)
# print('Scaler: ', scaler)
# =============================================================================

# =============================================================================
# MRVM
# model_name = 'MRVM'
# N = 10  # number of bidders
# M = 98  # number of items
# bidder_types = 3
# bidder_ids = list(range(0, 10))
# scaler = False # MinMaxScaler(feature_range=(0, 500))
# print('\nValue Model: ', model_name)
# print('Number of Bidders: ', N)
# print('Number of BidderTypes: ', bidder_types)
# print('Number of Items: ', M)
# print('Scaler: ', scaler)
# =============================================================================

# (2) Neural Network Parameters
epochs = 300
batch_size = 32
regularization_type = 'l2'  # 'l1', 'l2' or 'l1_l2'
# national bidder LSVM: id=0, GSVM:id=6, MRVM:id=7,8,9
regularization_N = 0.00001
learning_rate_N = 0.01
layer_N = [10, 10, 10]
dropout_N = True
dropout_prob_N = 0.05
# regional bidders LSVM: id=1-5, GSVM:id=0-5, MRVM:id=3,4,5,6
regularization_R = 0.00001
learning_rate_R = 0.01
layer_R = [32, 32]
dropout_R = True
dropout_prob_R = 0.05
# local bidders MRVM:id=0,1,2
regularization_L = 0.00001
learning_rate_L = 0.01
layer_L = [16, 16]
dropout_L = True
dropout_prob_L = 0.05
DNN_parameters = {}
if model_name == 'LSVM':
    for bidder_id in bidder_ids:
        if bidder_id == 0:
            DNN_parameters['Bidder_{}'.format(bidder_id)] = (regularization_N, learning_rate_N, layer_N, dropout_N, dropout_prob_N)
        else:
            DNN_parameters['Bidder_{}'.format(bidder_id)] = (regularization_R, learning_rate_R, layer_R, dropout_R, dropout_prob_R)
if model_name == 'GSVM':
    for bidder_id in bidder_ids:
        if bidder_id == 6:
            DNN_parameters['Bidder_{}'.format(bidder_id)] = (regularization_N, learning_rate_N, layer_N, dropout_N, dropout_prob_N)
        else:
            DNN_parameters['Bidder_{}'.format(bidder_id)] = (regularization_R, learning_rate_R, layer_R, dropout_R, dropout_prob_R)
if model_name == 'MRVM':
    for bidder_id in bidder_ids:
        if bidder_id in [0, 1, 2]:
            DNN_parameters['Bidder_{}'.format(bidder_id)] = (regularization_L, learning_rate_L, layer_L, dropout_L, dropout_prob_L)
        if bidder_id in [3, 4, 5, 6]:
            DNN_parameters['Bidder_{}'.format(bidder_id)] = (regularization_R, learning_rate_R, layer_R, dropout_R, dropout_prob_R)
        if bidder_id in [7, 8, 9]:
            DNN_parameters['Bidder_{}'.format(bidder_id)] = (regularization_N, learning_rate_N, layer_N, dropout_N, dropout_prob_N)
sample_weight_on = False
sample_weight_scaling = None
# =============================================================================
# sample_weight_on = True
# #sample_weight_scaling = [10, 10, 10, 10, 10, 30]  # example for GSVM
# sample_weight_scaling = [30, 10, 10, 10, 10, 10]  # example for LSVM
# #sample_weight_scaling = [10, 10, 10, 10, 10, 10, 10, 30, 30, 30]  # example for MRVM
# =============================================================================

print('\n------------------------ DNN  parameters ------------------------')
print('Epochs:', epochs)
print('Batch Size:', batch_size)
print('Regularization:', regularization_type)
for key in list(DNN_parameters.keys()):
    print(key, DNN_parameters[key])
print('Sample weighting:', sample_weight_on)
print('Sample weight scaling:', sample_weight_scaling)

# (3) MIP parameters
L = 3000
Mip_bounds_tightening = 'IA'   # False ,'IA' or 'LP'
warm_start = False
print('\n------------------------ MIP  parameters ------------------------')
print('Mip_bounds_tightening:', Mip_bounds_tightening)
print('Warm_start:', warm_start)

# (4) PVM specific parameters
caps = [30, 10] # [c_0, c_e] with initial bids c0 and maximal number of value queries ce
different_initial_bids_per_eco = False;
seed_instance = seed
min_iteration = 1
print('\n------------------------ PVM  parameters ------------------------')
print('c0:', caps[0])
print('ce:', caps[1])
print('Seed: ', seed_instance)
print('min_iteration:', min_iteration)
# %% Start DNN-based PVM

RESULT = pvm(scaler=scaler, caps=caps, L=L, parameters=DNN_parameters, epochs=epochs, batch_size=batch_size, model_name=model_name, sample_weight_on=sample_weight_on,
            sample_weight_scaling=sample_weight_scaling, min_iteration=min_iteration, seed_instance=seed_instance, regularization_type=regularization_type,
            Mip_bounds_tightening=Mip_bounds_tightening, warm_start=warm_start, different_initial_bids_per_eco=different_initial_bids_per_eco)


# %% Result analysis

# prepare results
EFF = RESULT[1][0]
ALLOCS = RESULT[1][1]
PAYMENTS = RESULT[1][2]
TIME_STATS = RESULT[1][3]
ITERATIONS = RESULT[1][4]
BOUNDS = RESULT[1][5]
MIP_STATS_TIME = RESULT[1][6]
mip_time = []
for k, v in MIP_STATS_TIME.items():
    mip_time = mip_time + v
Miptimes = pd.DataFrame(mip_time)
ACTUAL_QUERIES = RESULT[1][7]
Actualqueries = pd.DataFrame.from_dict(ACTUAL_QUERIES, orient='index')
Allocation_sats = pd.DataFrame.from_dict(ALLOCS[0]).transpose()
Allocation_sats = Allocation_sats.rename(index={k: 'Bidder_{}'.format(k) for k in range(0, N)})
sum_sats = sum(Allocation_sats['value'])
Allocation_pea = pd.DataFrame.from_dict(ALLOCS[1]).transpose()
Allocation_pea = Allocation_pea.rename(index={'Bidder_{}'.format(k): 'Bidder_{}'.format(k) for k in range(0, N)})
sum_pvm = sum(Allocation_pea['value'])
payments = pd.DataFrame.from_dict(PAYMENTS,  orient='index')
Rev_pos = payments[payments > 0].sum(axis=0).values[0]
Rev = payments.sum(axis=0).values[0]
RelRev_pos = Rev_pos/sum_sats*100
RelRev = Rev/sum_sats*100
total_bounds = pd.DataFrame.from_dict(BOUNDS, orient='index').describe()

# print results
print('---------------------------------  RESULT ---------------------------------------')
print('Valuation Model:', model_name)
print('Instance seed:', RESULT[0])
print('---------------------------------------------------------------------------------')
print('Efficiency in %: ', EFF)
print()
print('Payments:')
pays = []
for k, v in PAYMENTS.items():
    print(k, ':', v)
    pays.append(v)
print(pays)
print()
print('Relative Revenue in %: ', RelRev)
print('Relative Revenue (if payments capped at zero) in %: ', RelRev_pos)
print('\nIterations:')
sum_iterations = 0
max_iterations = 0
for k, v in ITERATIONS.items():
    print(k, ':', v)
    sum_iterations+=v
    max_iterations = max(max_iterations,v)
average_iterations = sum_iterations/(N+1)
print()
print('Instance Time (hh:mm:ss): ' + TIME_STATS)
print('Average MIP Time: {} sec'.format(round(Miptimes.describe().loc['mean'].mean(), 4)))
print('Median MIP Time: {} sec'.format(round(Miptimes.describe().loc['50%'].mean(), 4)))
print('Average #Queries per bidder - c0: {}'.format(Actualqueries.mean(axis=0).values[0]))
print('Average Maximium #Queries per bidder - c0: {}'.format(Actualqueries.max(axis=0).values[0]))
print('---------------------------------------------------------------------------------')
if model_name == 'LSVM':
    Na = ['Bidder_0']
    Re = ['Bidder_{}'.format(k) for k in range(1, 6)]
if model_name == 'GSVM':
    Na = ['Bidder_6']
    Re = ['Bidder_{}'.format(k) for k in range(0, 6)]
if model_name == 'MRVM':
    Na = ['Bidder_7', 'Bidder_8', 'Bidder_9']
    Re = ['Bidder_3', 'Bidder_4', 'Bidder_5', 'Bidder_6']
    Lo = ['Bidder_0', 'Bidder_1', 'Bidder_2']

print('SATS Social Welfare:', round(sum_sats, 2))
print('Allocation:')
print(Allocation_sats['good_ids'].to_string())
print('\nValues:')
if model_name == 'MRVM':
    print('Local bidders:')
    print(Allocation_sats['value'][Lo].to_string())
    ALocal = Allocation_sats['value'][Lo].sum()/sum_sats
    ALocal = round(ALocal*100, 2)
    print('% of SATS Social Welfare', ALocal, '\n')
print('Regional bidders:')
print(Allocation_sats['value'][Re].to_string())
AReg = Allocation_sats['value'][Re].sum()/sum_sats
AReg = round(AReg*100, 2)
print('% of SATS Social Welfare', AReg, '\n')
print('National bidders:')
print(Allocation_sats['value'][Na].to_string())
ANat = Allocation_sats['value'][Na].sum()/sum_sats
ANat = round(ANat*100, 2)
print('% of SATS Social Welfare', ANat)
print('---------------------------------------------------------------------------------')
print('PVM Social Welfare:', round(sum_pvm, 2))
print('Allocation:')
print(Allocation_pea['good_ids'].to_string())
print('\nValues:')
if model_name == 'MRVM':
    print('Local bidders:')
    print(Allocation_pea['value'][Lo].to_string())
    ALocal_pvm = Allocation_pea['value'][Lo].sum()/sum_sats
    ALocal_pvm = round(ALocal_pvm*100, 2)
    print('% of SATS Social Welfare', ALocal_pvm, '\n')
print('Regional bidders:')
print(Allocation_pea['value'][Re].to_string())
AReg_pvm = Allocation_pea['value'][Re].sum()/sum_sats
AReg_pvm = round(AReg_pvm*100, 2)
print('% of SATS Social Welfare', AReg_pvm, '\n')
print('National bidders:')
print(Allocation_pea['value'][Na].to_string())
ANat_pvm = Allocation_pea['value'][Na].sum()/sum_sats
ANat_pvm = round(ANat_pvm*100, 2)
print('% of SATS Social Welfare', ANat_pvm)
print('---------------------------------------------------------------------------------')



tmp = Allocation_sats['good_ids']
tmp2 = Allocation_pea['good_ids']

# Results for GSVM
if model_name=='GSVM':
    res = {'seed_instance': RESULT[0], 'efficiency' : EFF, 'pay_bidder_0' : pays[0], 'pay_bidder_1' : pays[1], 'pay_bidder_2': pays[2], 
    'pay_bidder_3' : pays[3], 'pay_bidder_4': pays[4], 'pay_bidder_5' : pays[5], 'pay_bidder_6' : pays[6], 'revenue' : RelRev, 'revenue_capped' : RelRev_pos, 'iter_max' : max_iterations, 
    'iter_average' : average_iterations, 'runtime' : TIME_STATS, 'average_MIP_time' : round(Miptimes.describe().loc['mean'].mean(), 4), 'median_MIP_time' : round(Miptimes.describe().loc['50%'].mean(), 4), 
    'avg_queries_per_bidder' : Actualqueries.mean(axis=0).values[0], 'avg_max_queries_per_bidder' : Actualqueries.max(axis=0).values[0], 'SATS_value' : sum_sats,  'percentage_regio_sats' : AReg,
    'percentage_national_sats' : ANat, 'bidder_0_sats' : tmp['Bidder_0'], 'bidder_1_sats' : tmp['Bidder_1'], 'bidder_2_sats' : tmp['Bidder_2'], 'bidder_3_sats' : tmp['Bidder_3'], 'bidder_4_sats' : tmp['Bidder_4'], 
    'bidder_5_sats' : tmp['Bidder_5'], 'bidder_6_sats': tmp['Bidder_6'], 'pvm_allocation_value' : sum_pvm, 'percentage_regio_pvm' : AReg_pvm, 'percentage_national_pvm' : ANat_pvm, 
    'bidder_0_pvm' : tmp2['Bidder_0'], 'bidder_1_pvm' : tmp2['Bidder_1'], 'bidder_2_pvm' : tmp2['Bidder_2'], 'bidder_3_pvm' : tmp2['Bidder_3'], 'bidder_4_pvm' : tmp2['Bidder_4'], 'bidder_5_pvm' : tmp2['Bidder_5']
    , 'bidder_6_pvm' : tmp2['Bidder_6'] }
 

# Results for LSVM
if model_name=='LSVM':
    res = {'seed_instance': RESULT[0], 'efficiency' : EFF, 'pay_bidder_0' : pays[0], 'pay_bidder_1' : pays[1], 'pay_bidder_2': pays[2], 
    'pay_bidder_3' : pays[3], 'pay_bidder_4': pays[4], 'pay_bidder_5' : pays[5], 'revenue' : RelRev, 'revenue_capped' : RelRev_pos, 'iter_max' : max_iterations, 
    'iter_average' : average_iterations, 'runtime' : TIME_STATS, 'average_MIP_time' : round(Miptimes.describe().loc['mean'].mean(), 4), 'median_MIP_time' : round(Miptimes.describe().loc['50%'].mean(), 4), 
    'avg_queries_per_bidder' : Actualqueries.mean(axis=0).values[0], 'avg_max_queries_per_bidder' : Actualqueries.max(axis=0).values[0], 'SATS_value' : sum_sats,  'percentage_regio_sats' : AReg,
    'percentage_national_sats' : ANat, 'bidder_0_sats' : tmp['Bidder_0'], 'bidder_1_sats' : tmp['Bidder_1'], 'bidder_2_sats' : tmp['Bidder_2'], 'bidder_3_sats' : tmp['Bidder_3'], 'bidder_4_sats' : tmp['Bidder_4'], 
    'bidder_5_sats' : tmp['Bidder_5'], 'pvm_allocation_value' : sum_pvm, 'percentage_regio_pvm' : AReg_pvm, 'percentage_national_pvm' : ANat_pvm, 
    'bidder_0_pvm' : tmp2['Bidder_0'], 'bidder_1_pvm' : tmp2['Bidder_1'], 'bidder_2_pvm' : tmp2['Bidder_2'], 'bidder_3_pvm' : tmp2['Bidder_3'], 'bidder_4_pvm' : tmp2['Bidder_4'], 'bidder_5_pvm' : tmp2['Bidder_5'] }

# Results for MRVM
if model_name=='MRVM':
    res = {'seed_instance': RESULT[0], 'efficiency' : EFF, 'pay_bidder_0' : pays[0], 'pay_bidder_1' : pays[1], 'pay_bidder_2': pays[2], 
    'pay_bidder_3' : pays[3], 'pay_bidder_4': pays[4], 'pay_bidder_5' : pays[5], 'pay_bidder_6' : pays[6], 'pay_bidder_7' : pays[7], 
    'pay_bidder_8' : pays[8], 'pay_bidder_9': pays[9], 'revenue' : RelRev, 'revenue_capped' : RelRev_pos, 'iter_max' : max_iterations, 
    'iter_average' : average_iterations, 'runtime' : TIME_STATS, 'average_MIP_time' : round(Miptimes.describe().loc['mean'].mean(), 4), 
    'median_MIP_time' : round(Miptimes.describe().loc['50%'].mean(), 4), 'avg_queries_per_bidder' : Actualqueries.mean(axis=0).values[0], 
    'avg_max_queries_per_bidder' : Actualqueries.max(axis=0).values[0], 'SATS_value' : sum_sats,  'percentage_regio_sats' : AReg,
    'percentage_national_sats' : ANat, 'percentage_local_sats' : ALocal, 'bidder_0_sats' : tmp['Bidder_0'], 'bidder_1_sats' : tmp['Bidder_1'], 
    'bidder_2_sats' : tmp['Bidder_2'], 'bidder_3_sats' : tmp['Bidder_3'], 'bidder_4_sats' : tmp['Bidder_4'], 'bidder_5_sats' : tmp['Bidder_5'], 
    'bidder_6_sats': tmp['Bidder_6'], 'bidder_7_sats' : tmp['Bidder_7'], 'bidder_8_sats' : tmp['Bidder_8'], 'bidder_9_sats' : tmp['Bidder_9'], 
    'pvm_allocation_value' : sum_pvm, 'percentage_regio_pvm' : AReg_pvm, 'percentage_national_pvm' : ANat_pvm, 'percentage_local_pvm': ALocal_pvm, 
    'bidder_0_pvm' : tmp2['Bidder_0'], 'bidder_1_pvm' : tmp2['Bidder_1'], 'bidder_2_pvm' : tmp2['Bidder_2'], 'bidder_3_pvm' : tmp2['Bidder_3'], 
    'bidder_4_pvm' : tmp2['Bidder_4'], 'bidder_5_pvm' : tmp2['Bidder_5'], 'bidder_6_pvm' : tmp2['Bidder_6'], 'bidder_7_pvm' : tmp2['Bidder_7'], 
    'bidder_8_pvm' : tmp2['Bidder_8'], 'bidder_9_pvm' : tmp2['Bidder_9'] }



results = results.append(res, ignore_index=True)
results.to_csv('/home/stkramer/bachelor_thesis/ba/results_lsvm_30_ali_sib.csv')


# print("OTHER ALLOCATIONS: ")
# other_allocations = RESULT[1][8]

# for k in other_allocations:
    # allocation = k[0]
    # for key in list(allocation.keys()):
        # print(allocation[key])
    # print('\nValue: %s \n', k[1])

# %% define logger


